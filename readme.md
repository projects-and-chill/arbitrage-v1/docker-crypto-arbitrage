# Launch Demo : 

:warning: Prerequis DEV nécéssaires :
- Avoir `Docker`
- Avoir l'utilitaire `make` (execution de **makefile**), cette commande est installée par defaut sur les OS linux tel que Ubuntu, Fedora, ... )
- Avoir l'utilitaire `nvm` 
- Avoir l'utilitaire `envsubst`

:warning: Prerequis DEMO nécéssaires :
- Avoir `Docker`
- Avoir l'utilitaire `make` (execution de **makefile**), cette commande est installée par defaut sur les OS linux tel que Ubuntu, Fedora, ... )

### :fast_forward:  Lancer l'application:

1. Lancer l'app localement  ( Clone services from git repo & execute docker-compose command)
```cmd
make up demo
```

2. Supprimer l'app et nettoyer ses volumes, images et networks
```cmd
make clear demo
```



# Development environment : 

### Launch dev environment

1. Init environment (only on first use ! )
```sh
make clone
make init
```
2. Launch environment
```sh
make up dev
```

### Clear dev environment 
- Remove all containers created by "make up dev"
```sh 
make down dev
```

- Clear all images, volumes, containers, networks created by "make up dev"
```sh 
make clear dev
```

