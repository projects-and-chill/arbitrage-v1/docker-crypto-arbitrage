ifeq (up,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif
ifeq (down,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif
ifeq (clear,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif

PROJECT_DIR = "./"
ENV_FILE = "./environments/$(RUN_ARGS)/.env.global"
COMPOSE_FILE_1 = "./docker-compose.yml"
COMPOSE_FILE_2 = "./environments/$(RUN_ARGS)/docker-compose.$(RUN_ARGS).yml"

#-------------------Init dev--------------------------

clone:
	git clone https://gitlab.com/projects-and-chill/arbitrage-v1/filter-symbols-service.git ./services/api-service
	git clone https://gitlab.com/projects-and-chill/arbitrage-v1/websocket-arbitrage.git ./services/apiws-service
	git clone https://gitlab.com/projects-and-chill/arbitrage-v1/admin-pannel-service.git ./services/front-service
	git clone https://gitlab.com/projects-and-chill/arbitrage-v1/mongo-arbitrage.git ./services/database-service
	git clone https://gitlab.com/projects-and-chill/arbitrage-v1/reverse-proxy-arbitrage.git ./services/reverse-proxy-service

init:
	npm --prefix ./services/api-service install
	npm --prefix ./services/apiws-service install
	. ${NVM_DIR}/nvm.sh && nvm use 14 && npm --prefix ./services/front-service install



#-------------------Up environment--------------------------
.PHONY: up
up:
	  docker-compose --project-directory $(PROJECT_DIR) --env-file $(ENV_FILE) -f $(COMPOSE_FILE_1)  -f $(COMPOSE_FILE_2) up



#-------------------Down environment--------------------------
.PHONY: down
down:
	  docker-compose --project-directory $(PROJECT_DIR) --env-file $(ENV_FILE) -f $(COMPOSE_FILE_1) -f $(COMPOSE_FILE_2) down -v


#-------------------Clear environment--------------------------
.PHONY: clear
clear:
	  docker-compose --project-directory $(PROJECT_DIR) --env-file $(ENV_FILE) -f $(COMPOSE_FILE_1) -f $(COMPOSE_FILE_2) down -v --rmi "all"
